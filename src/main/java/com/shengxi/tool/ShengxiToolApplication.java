package com.shengxi.tool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 * @author matthew
 * @date 2021-04-28 09:05:05
 */
@SpringBootApplication
public class ShengxiToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShengxiToolApplication.class, args);
    }

}
